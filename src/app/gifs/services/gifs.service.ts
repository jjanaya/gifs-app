import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGifsResponse } from '../interfaces/gifs.interfaces';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private apiKey    : string = 'hA514Ft9YUmdUC5UKUeZeUaQAhoQ4iaH';
  private _historial: string[] = [];
  private serviceUrl: string = 'http://api.giphy.com/v1/gifs'; 
  public resultados : Gif[] = [];

  get historial () {
    return [...this._historial];
  }

  constructor( private http:HttpClient){

   this._historial = JSON.parse(localStorage.getItem('historial')!) || [];
   this.resultados =  JSON.parse(localStorage.getItem('resultados')!) || [];

  }

  buscarGifs (query:string= ''){

    if (query.trim()=='')
      return;

      query = query.trim().toLocaleLowerCase();

    if (!this._historial.includes(query)){
      this._historial.unshift(query);
      this._historial= this._historial.splice(0,10);

      localStorage.setItem('historial', JSON.stringify(this._historial));
    }

    const params = new HttpParams()
          .set('api_key', this.apiKey)
          .set('limit', '12')
          .set('q', query)

    this.http.get<SearchGifsResponse>(`${this.serviceUrl}/search`, {params})
      .subscribe ( (resp) => {
        this.resultados= resp.data;
        localStorage.setItem('resultados', JSON.stringify(this.resultados));
      })
  }




}
